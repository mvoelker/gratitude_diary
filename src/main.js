import { createApp } from 'vue'
import App from './App.vue'
import './index.css'
import {themeChange} from "theme-change"
import { createI18n } from 'vue-i18n'
import messages from '@intlify/vite-plugin-vue-i18n/messages'

const i18n = createI18n({
  legacy: false,
  locale: 'de',
  messages,
  globalInjection: true
})
themeChange()
createApp(App).use(i18n).mount('#app')
