from pathlib import Path

import flask
from flask import request
from flask_cors import CORS
import json

app = flask.Flask(__name__)
CORS(app)
DATA_FILE = Path(Path(__file__).parent.resolve(), "data.json")

@app.route('/', methods=['GET'])
def home():
    return "<h1>Api server for diary database</h1>"

# A route to return all of the available entries in the json file.
@app.route('/api/all_entries', methods=['GET'])
def api_all():
    with open(DATA_FILE) as f:
        return f.read()

@app.route('/api/create', methods=['POST'])
def api_create():
    with open(DATA_FILE) as f:
        data = json.load(f)
    inData = json.loads(json.dumps(request.get_json()))
    inData.update({"id":len(data)})
    data.append(inData)
    with open(DATA_FILE, "w") as f:
        json.dump(data,f)
    return "Success", 200

