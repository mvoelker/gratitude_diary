# Gratitude Diary
## Intro
The application is used to write down grateful moment. The reflection of grateful moments will increase the happiness and satisfaction. 
The evidenz is given through the science topic positive psychology. 

+ [Berkley](https://ggsc.berkeley.edu/images/uploads/GGSC-JTF_White_Paper-Gratitude-FINAL.pdf)
+ [Psychologie Today](https://www.psychologytoday.com/us/basics/gratitude)

This web application is built with vue, tailwind and the beautiful daisy ui. 
Here is a list with implemented features:
+ Bi-lingual (Englisch and German)
+ Selectable Themes

In future I would like to implement more analytics and a possibility for an afterwards modification - e.g. for typos.

## How to start
### Build the container
The container is built with the docker_build_container.sh. 
### Start the application
Use the docker_start_container.sh for this.
Open __localhost:15002__ for the ui

## Technical details
### Backend
Is a gunicorn flask app to create new entries via a restapi von __localhost:15001__
### Variables
In the vars.js it is needed to set the names and also the path to images of the user. By default minions are implemented. 


