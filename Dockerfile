FROM alpine:3.14
RUN apk add python3 py3-pip nodejs npm
RUN pip install flask flask-cors gunicorn
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT /entrypoint.sh
